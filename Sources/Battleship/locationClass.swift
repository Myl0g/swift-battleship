enum TargetStatus {
    case unguessed, hit, missed
}

public final class Location {
    var hasShip = false
    var status: TargetStatus = .unguessed

    init() {}
}
