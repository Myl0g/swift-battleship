public final class Actor: CustomStringConvertible {
    let grid: Grid
    let name: String
    let cpu: Bool

    init(name: String, cpu: Bool) {
        self.cpu = cpu
        grid = Grid(rows: 10, columns: 10)
        self.name = name
    }

    func populate() {
        grid.populate(manual: !cpu)
    }

    func attack(opponent: Actor) -> TargetStatus {
        var row: Int
        var col: Int

        if !cpu {
            repeat {
                print("Enter row to attack:", terminator: " ")
                row = Int(readLine()!)! - 1
                print("Enter column to attack:", terminator: " ")
                col = Int(readLine()!)! - 1
            } while !opponent.grid.isValidCoordinate(row: row, column: col)
        } else {
            row = Int.random(in: 0 ..< opponent.grid.rows)
            col = Int.random(in: 0 ..< opponent.grid.columns)
        }

        let location = opponent.grid.grid[row][col]

        if location.hasShip {
            location.status = .hit
        } else {
            location.status = .missed
        }

        return location.status
    }

    public var description: String {
        return name
    }
}
