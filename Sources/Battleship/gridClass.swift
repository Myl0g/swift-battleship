public final class Grid: CustomStringConvertible {
    var grid: [[Location]] = []
    var ships: [Ship] = []
    let rows: Int
    let columns: Int

    init(rows: Int, columns: Int) {
        for _ in 0 ..< rows {
            var temp: [Location] = []
            for _ in 0 ..< columns {
                temp.append(Location())
            }
            grid.append(temp)
        }

        self.rows = grid.count
        self.columns = grid[0].count
    }

    var shipCount: Int {
        var living = ships.count
        for ship in ships {
            if ship.status == .hit {
                living -= 1
            }
        }
        return living
    }

    func isValidCoordinate(row: Int, column: Int) -> Bool {
        return ((row + 1) > 0 && (row + 1) <= rows) && ((column + 1) > 0 && (column + 1) <= columns)
    }

    func placeShip(ship: Ship, startingRow: Int, startingColumn: Int) {
        var locations = [grid[startingRow][startingColumn]]

        let facing: String
        if ship.direction == .horizontal {
            if (startingColumn + ship.length) > columns {
                facing = "left"
            } else {
                facing = "right"
            }
        } else {
            if (startingRow + ship.length) > rows {
                facing = "forward"
            } else {
                facing = "back"
            }
        }

        for i in 1 ..< ship.length {
            var row = startingRow; var col = startingColumn

            if ship.direction == .horizontal {
                if facing == "left" {
                    col -= i
                } else if facing == "right" {
                    col += i
                }
            } else {
                if facing == "back" {
                    row += i
                } else if facing == "forward" {
                    row -= i
                }
            }

            locations.append(grid[row][col])
        }

        ship.setSpaces(locations)
        ships.append(ship)
    }

    func randomlyPlace(desiredSpaces: [Int]) {
        var i = 0
        while i < desiredSpaces.count {
            let row = Int.random(in: 0 ..< rows)
            let col = Int.random(in: 0 ..< columns)

            if grid[row][col].hasShip {
                continue
            }

            let direction: Direction
            if Bool.random() {
                direction = .horizontal
            } else {
                direction = .vertical
            }

            placeShip(ship: Ship(length: desiredSpaces[i], direction: direction), startingRow: row, startingColumn: col)
            i += 1
        }
    }

    func askForShip(_ name: String, numSpaces: Int) {
        print(toString(displayShips: true))

        print("Now placing \(name.yellow) (\(numSpaces) spaces).")

        var row: Int = 0
        var col: Int = 0
        var dir: Direction = .vertical
        var acceptable = false

        while !acceptable {
            print("Enter desired direction (h or v):", terminator: " ")

            switch readLine()! {
            case "h":
                dir = .horizontal
            case "v":
                dir = .vertical
            default:
                print("Invalid direction. Try again.")
                continue
            }

            print("Enter starting row:", terminator: " ")
            row = Int(readLine()!)! - 1

            print("Enter starting column:", terminator: " ")
            col = Int(readLine()!)! - 1

            if !isValidCoordinate(row: row, column: col) {
                print("Your choice is invalid. Try again.".red)
                continue
            }

            if grid[row][col].hasShip {
                print("There is already a ship at that spot. Try again.".red)
                continue
            }
            acceptable = true
        }

        placeShip(ship: Ship(length: numSpaces, direction: dir), startingRow: row, startingColumn: col)
    }

    func populate(manual: Bool) {
        if !manual {
            randomlyPlace(desiredSpaces: [2, 3, 3, 4, 5])
        } else {
            /*
             Cruiser: 2 spaces
             Sub: 3
             Destroyer: 3
             Battleship: 4
             Aircraft: 5
             */

            askForShip("Cruiser", numSpaces: 2)
            askForShip("Sub", numSpaces: 3)
            askForShip("Destroyer", numSpaces: 3)
            askForShip("Battleship", numSpaces: 4)
            askForShip("Aircraft", numSpaces: 5)
        }
    }

    func toString(displayShips: Bool) -> String {
        var result = ""
        for i in 0 ..< rows {
            for j in 0 ..< columns {
                let current = grid[i][j]

                if displayShips && current.status == .unguessed && current.hasShip {
                    result += " # "
                    continue
                }

                if current.status == .unguessed {
                    result += " - "
                } else if current.status == .hit {
                    result += " X "
                } else {
                    result += " 0 "
                }
            }
            result += "\n"
        }
        return result
    }

    public var description: String {
        return toString(displayShips: true)
    }
}
