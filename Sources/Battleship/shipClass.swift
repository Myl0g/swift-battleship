enum Direction {
    case vertical, horizontal
}

public final class Ship {
    let length: Int
    let direction: Direction
    var spaces: [Location] = []

    init(length: Int, direction: Direction, spaces: [Location]) {
        self.length = length
        self.direction = direction
        setSpaces(spaces)
    }

    init(length: Int, direction: Direction) {
        self.length = length
        self.direction = direction
        spaces = []
    }

    func setSpaces(_ spaces: [Location]) {
        self.spaces = spaces
        for space in spaces {
            space.hasShip = true
        }
    }

    var status: TargetStatus {
        for space in spaces {
            if space.status == .unguessed {
                return .unguessed
            }
        }

        return .hit
    }
}
