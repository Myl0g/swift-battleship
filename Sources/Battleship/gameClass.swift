public final class Game {
    let player1: Actor
    let player2: Actor
    var turn: Int

    init() {
        player1 = Actor(name: "Player 1", cpu: false)
        player2 = Actor(name: "Computer", cpu: true)

        player1.populate()
        player2.populate()

        turn = 0
    }

    func canContinue() -> Bool {
        return player1.grid.shipCount != 0 && player2.grid.shipCount != 0
    }

    func takeTurn() {
        let attacker = turn == 1 ? player1 : player2
        let defender = turn == 0 ? player1 : player2
        print("\(attacker) attacks \(defender).".magenta)

        let result = attacker.attack(opponent: defender)
        switch result {
        case .hit:
            print("Ship hit!".onRed)
        default:
            print("Missed.".onBlue)
        }

        print(defender.grid.toString(displayShips: false))

        if turn == 1 {
            turn -= 1
        } else {
            turn += 1
        }
    }

    func declareWinner() {
        let winner = player1.grid.shipCount > player2.grid.shipCount ? player1 : player2

        print("The winner is \(winner)!".onBlue)
    }
}
