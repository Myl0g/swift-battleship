// Glibc is named "Darwin" under macOS. Since Swift can be compiled for both macOS and Linux, both are accomodated below.

#if os(Linux)
    import Glibc
#else
    import Darwin
#endif

import Rainbow

// Battleship 2018
// Copyright Milo J. Gilad

func battleship() {
    let game = Game()
    repeat {
        print(game.player1.grid)
        print("\(game.player1.description.green) has \(String(game.player1.grid.shipCount).yellow) ships left and \(game.player2.description.red) has \(String(game.player2.grid.shipCount).yellow) ships left.")
        sleep(3)
        game.takeTurn()
        sleep(3)
    } while game.canContinue()

    game.declareWinner()
}

battleship()
